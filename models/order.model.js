const mongoose = require('mongoose');

var order = new mongoose.Schema({
    orderData: {
        price: {
            type: Number
        },
        country: {
            type: String
        },    
        deliveryMethod: {
            type: String
        },
        street: {
            type: String
        },
        email: {
            type: String
        },
        name: {
            type: String
        }
    },
    ingredients: {
        salad: {
            type: String
        },
        bacon: {
            type: String
        },
        cheese: {
            type: String
        },    
        meat: {
            type: String
        }
    }
})

module.exports = mongoose.model('order', order);