const mongoose = require('mongoose');
const db = require("../config/settings").mongoURI;

mongoose.connect(db, {useNewUrlParser: true}, (err) => {
    if(!err) {
        console.log("MongoDB Connection Succeeded");

    }
    else {console.log(err)}
});
