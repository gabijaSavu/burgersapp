import React, { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import Layout from './hoc/Layout/Layout';
import BurgerBuilder from './containers/BurgerBuilder/BurgerBuilder';
import Checkout from './containers/Checkout/Checkout';
import Orders from './containers/Orders/Orders';
import SignUp from './components/Auth/Signup';
import Auth from './containers/Auth/Auth';
import Logout from './containers/Auth/Logout';

class App extends Component {



  render () {
    const PrivateRoute = ({component: Component, authed, ...rest}) => {
      return (
        <Route
          {...rest}
          render={(props) => (localStorage.getItem("jwtToken") !== null)
            ? <Component {...props} />
            : <Redirect to={{pathname: '/auth', state: {from: props.location}}} />}
        />
      )
    }



    return (
      <div>
        <Layout isAuthenticated={localStorage.getItem("jwtToken") !== null}>
          <Switch>
          <Route path="/auth" component={Auth} />
            <Route path="/" exact component={BurgerBuilder} />
            <PrivateRoute path="/checkout" component={Checkout} />
            <PrivateRoute path="/orders" component={Orders} />
            <PrivateRoute path="/logout" component={Logout} />
          </Switch>
        </Layout>
      </div>
    );
  }
}

export default App;
