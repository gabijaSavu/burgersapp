import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import axios from "axios";
import Input from '../../components/UI/Input/Input';
import Button from '../../components/UI/Button/Button';
import Spinner from '../../components/UI/Spinner/Spinner';
import classes from './Auth.css';

class Auth extends Component {
    state = {
        controls: {
            email: {
                elementType: 'input',
                elementConfig: {
                    type: 'email',
                    placeholder: 'Mail Address'
                },
                value: '',
                validation: {
                    required: true,
                    isEmail: true
                },
                valid: false,
                touched: false
            },
            password: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'Password'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 6
                },
                valid: false,
                touched: false,
                isAuthenticated: false,
                user: {},
                loading: false,
                isSignup: true
            }
        },
        
    }

   

    checkValidity ( value, rules ) {
        let isValid = true;
        if ( !rules ) {
            return true;
        }

        if ( rules.required ) {
            isValid = value.trim() !== '' && isValid;
        }

        if ( rules.minLength ) {
            isValid = value.length >= rules.minLength && isValid
        }

        if ( rules.maxLength ) {
            isValid = value.length <= rules.maxLength && isValid
        }

        if ( rules.isEmail ) {
            const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            isValid = pattern.test( value ) && isValid
        }

        if ( rules.isNumeric ) {
            const pattern = /^\d+$/;
            isValid = pattern.test( value ) && isValid
        }

        return isValid;
    }

    inputChangedHandler = ( event, controlName ) => {
        const updatedControls = {
            ...this.state.controls,
            [controlName]: {
                ...this.state.controls[controlName],
                value: event.target.value,
                valid: this.checkValidity( event.target.value, this.state.controls[controlName].validation ),
                touched: true
            }
        };
        this.setState( { controls: updatedControls } );
    }

    submitHandler = ( event ) => {
        event.preventDefault();

        let user = {
            username: "buger@burger.com",
            password: "burger"
        }
        // axios.post("/register", user)
        //     .then(this.props.history.push("/login")) // re-direct to login on successful register
        //     .catch(err =>
        //         console.log(err)
        //     );

        // if(!this.state.isSignup) {
        //     fetch("http://localhost:8000/register", {
        //         method: 'POST', 
        //         body: JSON.stringify({
        //             username: "buger@burger.com",
        //             password: "burger"
        //         }), 
        //         headers:{
        //             'Content-Type': 'application/json'
        //         }
        //         }).then(res => res.json())
        //         .then(response => { 
        //             console.log('Success:', JSON.stringify(response)),
        //             this.setState( { loading: false } );
        //         // this.props.history.push("/")
        //         })
        //         .catch(error => {
        //             console.error('Error:', error);
        //         });
        // }
        // else {

            fetch("http://localhost:8000/login", {
                method: 'POST', 
                body: JSON.stringify({
                    username: "buger@burger.com",
                    password: "burger"
                }), 
                headers:{
                    'Content-Type': 'application/json'
                }
                }).then(res => res.json())
                .then(response => { 
                    localStorage.setItem('jwtToken', response.token);
                    //this.setState({ message: '' });
                    console.log(response)
                    this.props.history.push('/')
                })
                .catch(error => {
                    console.error('Error:', error);
                });
            
                // axios.post('/login', { user })
                // .then((result) => {
                //     localStorage.setItem('jwtToken', result.data.token);
                //     console.log("registered")
                //     this.props.history.push('/')
                // })
                // .catch((error) => {
                //     if(error.response.status === 401) {
                //     console.log('Login failed. Username or password not match') 
                //     }
                // });
        // }

        

    
    }

    switchAuthModeHandler = () => {
        this.setState(prevState => {
            return {isSignup: !prevState.isSignup};
        });
    }

    render () {
        const formElementsArray = [];
        for ( let key in this.state.controls ) {
            formElementsArray.push( {
                id: key,
                config: this.state.controls[key]
            } );
        }

        let form = formElementsArray.map( formElement => (
            <Input
                key={formElement.id}
                elementType={formElement.config.elementType}
                elementConfig={formElement.config.elementConfig}
                value={formElement.config.value}
                invalid={!formElement.config.valid}
                shouldValidate={formElement.config.validation}
                touched={formElement.config.touched}
                changed={( event ) => this.inputChangedHandler( event, formElement.id )} />
        ) );

        // if (this.props.loading) {
        //     form = <Spinner />
        // }

        // let errorMessage = null;

        // if (this.props.error) {
        //     errorMessage = (
        //         <p>{this.props.error.message}</p>
        //     );
        // }

        // let authRedirect = null;
        // if (this.props.isAuthenticated) {
        //     authRedirect = <Redirect to={this.props.authRedirectPath}/>
        // }

        return (
            <div className={classes.Auth}>
                {/* {authRedirect} */}
                {/* {errorMessage} */}
                <form onSubmit={this.submitHandler}>
                    {form}
                    <Button btnType="Success">{this.state.isSignup ? 'SIGNUP' : 'SIGNIN'}</Button>
                </form>
                {<Button 
                    clicked={this.switchAuthModeHandler}
                    btnType="Danger">SWITCH TO {this.state.isSignup ? 'SIGNIN' : 'SIGNUP'}</Button>
                }
            </div>
        );
    }
}

export default Auth;