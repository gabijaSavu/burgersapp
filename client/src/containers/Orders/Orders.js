import React, { Component } from 'react';

import Order from '../../components/Order/Order';
import axios from '../../axios-orders';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';

class Orders extends Component {
    state = {
        orders: [],
        loading: true,
        ingredients: [],
        price: 0,
        id: 1
    }

    componentDidMount() {
        // axios.get('/orders.json')
        //     .then(res => {
        //         const fetchedOrders = [];
        //         for (let key in res.data) {
        //             fetchedOrders.push({
        //                 ...res.data[key],
        //                 id: key
        //             });
        //         }
        //         this.setState({loading: false, orders: fetchedOrders});
        //     })
        //     .catch(err => {
        //         this.setState({loading: false});
        //     });

            fetch('http://localhost:8000/order')
            .then(response => 
                 response.json())

            .then(data => 
                this.setState( { ingredients: {
                    salad: parseInt(data.ingredients.salad, 10),
                    bacon: parseInt(data.ingredients.bacon, 10),
                    cheese: parseInt(data.ingredients.cheese, 10),
                    meat: parseInt(data.ingredients.meat, 10) 
                    }, price: data.price

                }))

            .catch(error => this.setState({ loading: false }));
    }

    render () {
        return (
            <div>
                {/* {this.state.orders.map(order => ( */}
                    <Order 
                        key={this.state.id}
                        ingredients={this.state.ingredients}
                        price={this.state.price} />
                {/* ))} */}
            </div>
        );
    }
}

export default withErrorHandler(Orders, axios);