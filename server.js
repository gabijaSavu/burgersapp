require('./models/db');
let express = require('express')
let app = express()
let orderRoute = require('./routes/order')
let bodyParser = require('body-parser')
let authRoute = require('./routes/auth');
require('./config/passport');

app.use(bodyParser.json())

app.use((req, res, next) => {
  console.log(`${new Date().toString()} => ${req.originalUrl}`, req.body)
  next()
})

app.use(orderRoute)
app.use(authRoute)
app.use(express.static('public'))

app.use((req, res, next) => {
  res.status(404).send('The path was not found')
})

app.use((err, req, res, next) => {
  console.error(err.stack)
  //res.sendFile(path.join(__dirname, '../public/500.html'))
})

// const PORT = process.env.PORT || 8000
const PORT = 8000
app.listen(PORT, () => console.info(`Server has started on ${PORT}`))