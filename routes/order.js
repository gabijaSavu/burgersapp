const express = require('express');

let OrderModel = require('../models/order.model');
var router = express.Router();

router.post('/order', (req, res) => {
  if(!req.body) {
    return res.status(400).send('Request body is missing')
  }
  
  let model = new OrderModel(req.body)
  model.save()
    .then(doc => {
      if(!doc || doc.length === 0) {
        return res.status(500).send(doc)
      }

      res.status(201).send(doc)
    })
    .catch(err => {
      res.status(500).json(err)
    })
})

router.get('/order', (req, res) => {

    OrderModel.findOne()
      .then(doc => {
        res.json(doc)
      })
      .catch(err => {
        res.status(500).json(err)
      })
  })

  module.exports = router;